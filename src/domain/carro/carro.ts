export class Carro{

    public id: number = null;
    public tipo: string = '';
    public nome: string = '';
    public desc: string = '';
    public urlFoto: string = '';
    public urlVideo: string = '';
    public latitude: string = '';
    public longitude: string = '';
    
    constructor() {}
}